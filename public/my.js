let aboutMe = " Hello world! My name is Elizabeth Manemann. I am currently a Red Team Operator at Deloitte Consulting and am finishing up my Bachelor's Degree in Computer Science at the University of Missouri-Kansas City. I began my cybersecurity journey on the Blue Team at H&R block, where I began as a SOC analyst and then transitioned into the role of a SIEM engineer. During that time, I developed a passion for offensive security. I was selected to join the Deloitte Red Team in 2021 as a trainee, and quickly grew into the role of a Red Team Operator. During that time, I also earned my OSCP (Offensive Security Certified Professional) certification and am now working on obtaining the OSEP (Offensive Security Experienced Penetration Tester). ";

function abtMe() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';
    var image = document.createElement('img');
    image.src = './assets/profile.jpg';
    image.alt = 'Profile Picture';
    image.style.display = 'block';
    image.classList.add('profile-image');
    var content = document.createElement('p');
    content.textContent = aboutMe;
    content.classList.add('profile-text');

    terminal.appendChild(image);
    terminal.appendChild(content);
    
}

function resume() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';
  /*   var resumeDoc = document.createElement('a');
    resumeDoc.href = './assets/Elizabeth_Manemann_Resume.pdf';
    resumeDoc.target = '_blank';
    resumeDoc.textContent = 'Click here to download my resume as a PDF'
    resumeDoc.style.fontWeight = 'bold'; */

var resumehtml = `

<a href="./assets/Elizabeth_Manemann_Resume.pdf" style="fontWeight:bold" target="_blank"> Click here to view the PDF format. </a>

<p><b>Elizabeth Manemann</b></p>
<p><b>&#160;</b></p>
<p>Liberty,&#160;MO&#160;&#160;(816)456-3826&#160;&#160;elizabethmanemann@gmail.com</p>
<p>&#160;</p>
<p><b>Summary</b></p>
<p><b>&#160;</b></p>
<p>Utilize&#160;my problem-solving,&#160;logic-based,&#160;and&#160;technical&#160;skills&#160;to&#160;harden an&#160;organization’s&#160;<br/>cybersecurity posture&#160;and&#160;enhance&#160;a&#160;proactive&#160;cyber&#160;defense&#160;program.&#160;Additionally,&#160;to&#160;<br/>advance&#160;a&#160;company’s&#160;overall&#160;security approach&#160;by becoming&#160;an&#160;integral&#160;Red&#160;Team&#160;member&#160;<br/>that&#160;relentlessly questions&#160;assumptions&#160;and&#160;fosters&#160;an&#160;innovative,&#160;offensive&#160;mindset.</p>
<p>&#160;</p>
<p>&#160;</p>
<p><b>Experience&#160;</b></p>
<p><b>Security&#160;Engineer</b>,&#160;H&amp;R&#160;Block</p>
<p>&#160;</p>
<p>December&#160;2019&#160;–&#160;Present</p>
<p>&#160;</p>
<p>•&#160;&#160;Led&#160;SIEM&#160;migration&#160;from&#160;LogRhythm&#160;to&#160;Devo,&#160;completed&#160;in&#160;4&#160;months&#160;<br/>•&#160;&#160;Utilized&#160;the&#160;MITRE&#160;ATT&amp;CK&#160;Framework&#160;to&#160;build&#160;detection&#160;capabilities&#160;into&#160;SIEM&#160;<br/>•&#160;&#160;Developed&#160;creative&#160;solutions&#160;for&#160;SIEM&#160;infrastructure&#160;health&#160;monitoring&#160;<br/>•&#160;&#160;Designed&#160;SIEM&#160;architectural&#160;diagrams&#160;<br/>•&#160;&#160;Improved&#160;Logstash&#160;configuration&#160;to&#160;categorize&#160;syslog&#160;sources&#160;<br/>•&#160;&#160;Maintained&#160;proprietary syslog&#160;collectors&#160;on&#160;Linux&#160;and&#160;Windows&#160;servers&#160;<br/>•&#160;&#160;Enhanced&#160;log&#160;analysis&#160;via&#160;additional&#160;contextualization&#160;and&#160;parsing&#160;with&#160;Regex&#160;and&#160;</p>
<p>SQL&#160;queries&#160;</p>
<p>•&#160;&#160;Collaborated&#160;with&#160;security-adjacent&#160;teams&#160;to&#160;utilize&#160;SIEM&#160;feature&#160;set&#160;<br/>•&#160;&#160;Tuned&#160;SIEM&#160;alerts&#160;to&#160;detect&#160;relevant&#160;threat&#160;actor&#160;characteristics&#160;&#160;<br/>•&#160;&#160;Onboarded&#160;additional&#160;log&#160;sources&#160;to&#160;broaden SOC&#160;coverage&#160;&#160;&#160;<br/>•&#160;&#160;Trained&#160;offshore&#160;engineers&#160;in&#160;relevant&#160;tools&#160;and&#160;solution&#160;research&#160;</p>
<p><b>SOC Analyst</b>,&#160;H&amp;R&#160;Block</p>
<p>&#160;</p>
<p>September&#160;2019&#160;–&#160;December&#160;2019</p>
<p>&#160;</p>
<p>•&#160;&#160;Pioneered&#160;development&#160;of&#160;the&#160;Threat&#160;Intel program&#160;<br/>•&#160;&#160;Investigated&#160;and&#160;solved&#160;over&#160;50&#160;cases&#160;&#160;<br/>•&#160;&#160;Managed&#160;and&#160;coordinated&#160;weekly meetings&#160;with&#160;offshore&#160;analysts&#160;&#160;</p>
<p><b>SOC&#160;Intern</b>,&#160;H&amp;R&#160;Block</p>
<p>&#160;</p>
<p>June&#160;2019&#160;–&#160;September&#160;2019</p>
<p>&#160;</p>
<p>•&#160;&#160;Installed&#160;Elasticsearch&#160;cluster&#160;lab&#160;environment&#160;<br/>•&#160;&#160;Investigated&#160;and&#160;solved&#160;over&#160;40&#160;phishing&#160;email&#160;cases&#160;&#160;&#160;<br/>•&#160;&#160;Received&#160;training&#160;and&#160;exposure&#160;to&#160;various&#160;cybersecurity&#160;disciplines,&#160;including&#160;red&#160;</p>
<p>team,&#160;incident&#160;response,&#160;engineering,&#160;and&#160;application&#160;security&#160;</p>
<p><b>Achievements&#160;</b></p>
<p>•&#160;&#160;Devo&#160;Certified&#160;Platform&#160;User&#160;–&#160;April&#160;2021&#160;<br/>•&#160;&#160;Microsoft&#160;Certified:&#160;Azure&#160;Fundamentals&#160;–&#160;April&#160;2020&#160;<br/>•&#160;&#160;LogRhythm&#160;Platform&#160;Administrator&#160;Certification&#160;–&#160;February&#160;2020&#160;<br/>•&#160;&#160;Missouri&#160;Bright&#160;Flight&#160;Scholarship&#160;–&#160;May 2018&#160;<br/>&#160;</p>
<p><b>Education&#160;</b></p>
<p><b>B.S. Computer&#160;Science&#160;Candidate</b>&#160;</p>
<p>University of&#160;Missouri-Kansas&#160;City,&#160;Kansas&#160;City,&#160;MO</p>
<p>&#160;</p>
<p>Expected&#160;Graduation:&#160;May 2023</p>
<p>&#160;</p>
<p>&#160;</p>
`;

    // terminal.appendChild(resumeDoc);
    terminal.innerHTML = resumehtml;
    // terminal.appendChild(resumehtml);

   adjustTerminalHeight();
    
}

function classes() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';

var resumehtml = `

<h1>Education Background</h1>
<br>
<p>I graduated from Kearney High School in 2018, where I qualified for the A+ Program and achieved the Bright Flight scholarship. I then attended Metropolitan Community College (Maple Woods) for my first two years of college. I completed all of my General Education classes there. I then transferred to UMKC, where I am currently a senior with only one semester left until graduation. </p>
<br>
<h2 style="color: rgb(51, 255, 0);">Completed Undergraduate Courses</h2>
<br>
<h3>Metropolitan Community College (Maple Woods) </h3>
<br>
<p>First-Year Seminar</p>
<p>Composition & Reading I</p>
<p>United States History to 1865</p>
<p>Statistics</p>
<p>Intro to Philosophy</p>
<p>General Psychology </p>
<p>General Biology </p>
<p>Programming Fundamentals (C++)</p>
<p>Composition & Reading II</p>
<p>Trigonometry </p>
<p>Object-Oriented Progamming (C++)</p>
<p>Modern Western Civilization</p>
<p>Analytic Geometry & Calculus I</p>
<p>Music Appreciation </p>
<p>Discrete Struct for Comp Sci I</p>
<p>Analytic Geometry & Calc II</p>
<p>Engineering Physics I</p>
<br>
<h3>University of Missouri-Kansas City (UMKC)</h3>
<p>Discourse III [DISC 300]</p>
<p>Ethical Issues-Comp/Eng [ANCH 308]</p>
<p>Intro to Computer Architecture and Orginization [COMP-SCI 281R]</p>
<p>Applied Probability [COMP-SCI 394R]</p>
<p>Human Computer Interface [COMP-SCI 456]</p>
<p>Intro to Operating Systems [COMP-SCI 431]</p>
<p>Foundations of Software Engineering [COMP-SCI 449]</p>
<p>Intro to Database Management Systems [COMP-SCI 470]</p>
<p>Data Communication and Networking [COMP-SCI 320]</p>
<p>Intro to Artificial Intelligence [COMP-SCI 461]</p>
<p>Software Engineering Capstone [COMP-SCI 451R]</p>
<p>Distributed Computing Systems [COMP-SCI 446]</p>
<br>
<h2 style="color: rgb(51, 255, 0);">In-Progress</h2>
<h3>University of Missouri-Kansas City (UMKC)</h3>
<p>Special Topics (Web Development) [COMP-SCI 490WD]</p>
<p>Intro to Algorithms and Complex [COMP-SCI 404]</p>
<br>
`;

    // terminal.appendChild(resumeDoc);
    terminal.innerHTML = resumehtml;
    // terminal.appendChild(resumehtml);

   adjustTerminalHeight();
    
}

function credits() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';

var credits = `
    Template created by: R Narayan<br><a href = 'https://github.com/bitoffabyte' target='_blank'>https://github.com/bitoffabyte</a><br>
`;

    // terminal.appendChild(resumeDoc);
    terminal.innerHTML = credits;
    // terminal.appendChild(resumehtml);

   adjustTerminalHeight();
    
}

function skills() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';

var credits = `
    <h2 style="color: rgb(51, 255, 0);"> Skills </h2>
    <ul>
        <li>Proficent in Linux system administration</li>
        <li>Can perform network and log analysis </li>
        <li> Able to perform Red Team operations </li>
        <li> Can deploy an ADO pipeline to compile C# tools </li>
    </ul>
    <h2 style="color: rgb(51, 255, 0);"> Projects </h2>
    <ul>
        <li>Hacking lab in Azure</li>
        <li>Plex media server </li>
        <li>Red Team tool collection in ADO </li>
    </ul>
`;

    // terminal.appendChild(resumeDoc);
    terminal.innerHTML = credits;
    // terminal.appendChild(resumehtml);

   adjustTerminalHeight();
    
}


function contact() {
    var terminal = document.getElementById('term-content');
    terminal.innerHTML = '';

var credits = `
    <h2 style="color: rgb(51, 255, 0);">Contact Info</h2>
    <p><a href = 'https://www.linkedin.com/in/elizabeth-manemann-5316991a5/'>LinkedIn</a></p>
    <p>Email: elizabethmanemann@gmail.com</p>
`;

    // terminal.appendChild(resumeDoc);
    terminal.innerHTML = credits;
    // terminal.appendChild(resumehtml);

   adjustTerminalHeight();
    
}

function adjustTerminalHeight() {
    var terminal = document.querySelector('.terminal');
    var content = document.querySelector('#term-content');
  
    // Clear any inline height styles
    terminal.style.height = '';
  
    // Get the computed height of the content
    var contentHeight = content.offsetHeight;
  
    // Set the height of the terminal to match the content height
    terminal.style.height = contentHeight + 'px';
  }



